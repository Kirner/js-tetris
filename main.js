//Constants
var can = document.getElementById('canvas');
var ctx = can.getContext('2d');
var size = 40;
var speed = 500; //Speed in ms

//Variables
var grid = createGrid(10, 16); //Create 10 by 16 multi dimensional array. 
var lastUpdate = 0;
var lastRender = 0;

var tetromino = {
	shape: [[1,1,0],[0,1,1]], 
	topLeft:{row: 0, col:3}, 
	nextTopLeft:{row: 0, col:3}
}

var keyMap = {
	39: 'right',
	37: 'left',
	38: 'up',
	40: 'down'
}

var pressedKeys = {
	left: false,
	right: false,
	up: false,
	down: false
}

/* ===================================================== *
 * ----------------------- Setup ----------------------- *
 * ===================================================== */

//Returns a 2 dimensional array of a give width and height with all elements set to 0.
function createGrid(gridWidth, gridHeight){
	var grid = new Array(gridWidth);

	for(var x = 0; x < gridWidth; x++)
	{
		grid[x] = new Array(gridHeight);
		for(var y = 0; y < gridHeight; y++)
		{
			grid[x][y] = false;
		}
	}

	return grid;
}

//Creates a HTML table of a given height and width.
function drawGrid(boxesX, boxesY){
	var lenX = boxesX * size;
	var lenY = boxesY * size;
	can.height = boxesY * size;
	can.width = boxesX * size;
	

	ctx.beginPath();
	ctx.moveTo(0, 0 );
	ctx.lineTo(lenX, 0 );
	ctx.moveTo(0, 0 );
	ctx.lineTo(0, lenY);
	ctx.stroke();

	for (var i = 0; i < boxesY; i++) {
		ctx.beginPath();
		ctx.moveTo(0, size + size * i - .5);
		ctx.lineTo(lenX, size + size * i - .5);
		ctx.stroke();
	}

	for (var r = 0; r < boxesX; r++){
		ctx.beginPath();
		ctx.moveTo(size + size * r - .5, 0);
		ctx.lineTo(size + size * r - .5, lenY);
		ctx.stroke();
	}
}

/* ===================================================== *
 * ---------------------- Update ----------------------- *
 * ===================================================== */

//Checks for a collision at the "nextTopLeft" of a tetromino, if there is none "topLeft" 
//is set to the value of "nextTopLeft".   
function checkCollision(){
	var safe = true;
	var wall = false;

	for (var row = 0; row < tetromino.shape.length; row++) {
		for (var col = 0; col < tetromino.shape[row].length; col++) {
			var y = tetromino.nextTopLeft.row + row;
			var x = tetromino.nextTopLeft.col + col;
			
			if(tetromino.nextTopLeft.row == tetromino.topLeft.row){
				if(x < 0 || x > 9){
					wall = true;
				} 
				else if(grid[x][y] && (tetromino.shape[row][col] != 0)){
					wall = true;
				}
			}
			else{
				if(y > 15 ){
					safe = false;
				}
				else if(grid[x][y] && (tetromino.shape[row][col] != 0)){
					safe = false;
				}
			}
		}
	}
		
	if(wall){
		tetromino.nextTopLeft.col = tetromino.topLeft.col;
	}
	if(safe){
		tetromino.topLeft.row = tetromino.nextTopLeft.row;
		tetromino.topLeft.col = tetromino.nextTopLeft.col;
		return true;
	}
	else{
		return false;
	}
}

//Sets the totromino in the "landed" grid
function setTetromino(){
	//Looping through every block in the tetromino and set that index in the grid to true.
	for (var row = 0; row < tetromino.shape.length; row++) {
		for (var col = 0; col < tetromino.shape[row].length; col++) {
			if (tetromino.shape[row][col] != 0) {
				var y = tetromino.topLeft.row + row
				var x = tetromino.topLeft.col + col;
				grid[x][y] = true;
			}
		}
	}
}

//Randomizes the tetromino.
function newTetromino(){
	var rand = Math.floor(Math.random() * 8) + 1;	
	switch(rand){
		case 1: //I-block
			tetromino.shape = [[1,1,1,1]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
		case 2: //J-block
			tetromino.shape = [[1,0,0],[1,1,1]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
		case 3: //L-block
			tetromino.shape = [[0,0,1],[1,1,1]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
		case 4: //O-block
			tetromino.shape = [[1,1],[1,1]];
			tetromino.topLeft = {row: 0, col: 4}
			tetromino.nextTopLeft = {row: 0, col: 4}
			break;
		case 5: //S-block
			tetromino.shape = [[0,1,1],[1,1,0]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
		case 7: //T-block
			tetromino.shape = [[0,1,0],[1,1,1]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
		case 8: //Z-block
			tetromino.shape = [[1,1,0],[0,1,1]];
			tetromino.topLeft = {row: 0, col: 3}
			tetromino.nextTopLeft = {row: 0, col: 3}
			break;
	}
}

/* ===================================================== *
 * ---------------------  Drawing  --------------------- *
 * ===================================================== */

//Loops trough a given tetromino and draws each block. 
function drawTetromino(){
	for (var row = 0; row < tetromino.shape.length; row++) {
		for (var col = 0; col < tetromino.shape[row].length; col++) {
			if (tetromino.shape[row][col] != 0) {
				var y = tetromino.topLeft.row + row;
				var x = tetromino.topLeft.col + col;

				var cellY = size * y;
				var cellX = size * x;
				var img = document.getElementById("red-block");
				ctx.drawImage(img,cellX,cellY,size-1,size-1);
			}
		}
	}
}

//Redraws the grid based of the array.
function updateGrid(lengthX, lengthY){
	for(var y = 0; y < lengthY; y++){
		for(var x = 0; x < lengthX; x++){
			if(grid[x][y]){
				var cellY = size * y;
				var cellX = size * x;
				var img = document.getElementById("orange-block");
				ctx.drawImage(img,cellX,cellY,size-1,size-1);
			}
			else{
				var cellY = size * y;
				var cellX = size * x;
				ctx.clearRect(cellX,cellY,size-1,size-1);
			}
		}
	}
}

/* ===================================================== *
 * --------------------  Game Loops -------------------- *
 * ===================================================== */

function update(progress) {
	//Runs every 500ms
	lastUpdate += progress;

	if(lastUpdate > speed){
		tetromino.nextTopLeft.row++; //Set the next position down one;

		if(!checkCollision()){
			setTetromino(); //Set the tetromino in the grid array.
			newTetromino(); //Randomize the tetromino. 
		}
		lastUpdate = 0;
	}

	if(pressedKeys.right){
		tetromino.nextTopLeft.col += 1;
		if(!checkCollision()){
			setTetromino(); //Set the tetromino in the grid array.
			newTetromino(); //Randomize the tetromino. 
		}
		pressedKeys.right = false;
	}
	if(pressedKeys.left){
		tetromino.nextTopLeft.col -= 1;
		if(!checkCollision()){
			setTetromino(); //Set the tetromino in the grid array.
			newTetromino(); //Randomize the tetromino. 
		}
		pressedKeys.left = false;
	}
	if(pressedKeys.up){
		pressedKeys.up = false;
	}
	if(pressedKeys.down){
		tetromino.nextTopLeft.row += 1;
		if(!checkCollision()){
			setTetromino(); //Set the tetromino in the grid array.
			newTetromino(); //Randomize the tetromino. 
		}
		pressedKeys.down = false;
	}
}

function draw() {
	updateGrid(10,16); //Redraw the grid.
	drawTetromino(); //redraw the tetromino in the new position.
}

function loop(timestamp) {
  var progress = timestamp - lastRender;

  update(progress);
  draw();

  lastRender = timestamp;
  window.requestAnimationFrame(loop);
}

/* ===================================================== *
 * ----------------------- Events ---------------------- *
 * ===================================================== */

function keydown(event) {
  var key = keyMap[event.keyCode];
  pressedKeys[key] = true;
}
function keyup(event) {
  var key = keyMap[event.keyCode];
  pressedKeys[key] = false;
}

function init(){
	drawGrid(10, 16); //Create a 10 by 16 html table.
	newTetromino(); //Randomizes tetromino
	ctx.fillStyle="red"; //Set draw colour to red
	drawTetromino(tetromino); //Draw the tetromino on the grid
  window.requestAnimationFrame(loop);
}

document.onload = init();
window.addEventListener("keydown", keydown, false);
window.addEventListener("keyup", keyup, false);
